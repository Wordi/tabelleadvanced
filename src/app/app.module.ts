import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ViewComponent } from './view/view.component';
import { InputComponent } from './input/input.component';
import { PreviewsService } from './share/preview.service';




@NgModule({
  declarations: [
    AppComponent,
    ViewComponent,
    InputComponent

  ],
  imports: [
    BrowserModule
  ],
  providers: [PreviewsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
