/****** funktioniert so nicht - keine Ahnung warum
export class Preview {
  constructor (public home: String, public guest: String,
    public homeUrl: String, public guestUrl: String,
  public date: Date, public loc: String, public desc: String) { }
}
*/

export class Preview {
  public home: String;
  public guest: String;
  public homeUrl: String;
  public guestUrl: String;
  public date: Date;
  public loc: String;
  public desc: String;


  constructor (home: String, guest: String, homeUrl: String,
     guestUrl: String,
      date: Date, loc: String, desc: String ) {

         this.home = home;
         this.guest = guest;
         this.homeUrl = homeUrl;
         this.guestUrl = guestUrl;
         this.date = date;
         this.loc = loc;
         this.desc = desc;

      }
}
