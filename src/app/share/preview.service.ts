import { Preview } from './preview.model';

export class PreviewsService {
  private Previews: Preview[] = [];

  getPreviews () {
    return this.Previews;
  }

  addToPreviews (item: Preview) {
    this.Previews.push(item);
  }

  removeFromPreviews (item: Preview) {
    this.Previews.splice(this.Previews.length-1, 1);
  }

}
