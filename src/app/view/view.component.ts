import { Component, OnInit, Input } from '@angular/core';

import { PreviewsService } from '../share/preview.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
previews: any = [];


    constructor(private previewsService: PreviewsService ) { }

    ngOnInit() {
      this.previews = this.previewsService.getPreviews();
    }
  }
