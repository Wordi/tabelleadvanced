import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Preview }  from '../share/preview.model';
import { PreviewsService } from '../share/preview.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
neuesPreview: Preview;


@ViewChild('heim') heim: ElementRef;
@ViewChild('gast') gast: ElementRef;
@ViewChild('heimUrl') heimUrl: ElementRef;
@ViewChild('gastUrl') gastUrl: ElementRef;
@ViewChild('datum') datum: ElementRef;
@ViewChild('ort') ort: ElementRef;
@ViewChild('schreiben') schreiben: ElementRef;


addPreview(): void {
  this.neuesPreview = new Preview(this.heim.nativeElement.value, this.gast.nativeElement.value,
   this.heimUrl.nativeElement.value, this.gastUrl.nativeElement.value, this.datum.nativeElement.value,
 this.ort.nativeElement.value, this.schreiben.nativeElement.value);
 this.previewsService.addToPreviews(this.neuesPreview);
 console.log(this.neuesPreview)
}

erasePreview(): void {
  this.previewsService.removeFromPreviews(this.neuesPreview);
}



  constructor(private previewsService: PreviewsService ) { }

  ngOnInit() {

  }
}
