import { AusprobierenPage } from './app.po';

describe('ausprobieren App', () => {
  let page: AusprobierenPage;

  beforeEach(() => {
    page = new AusprobierenPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
